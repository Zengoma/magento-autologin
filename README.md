#zengoma/magento-autologin

##Description:
An ultra lightweight module that automatically logs a customer in after account creation. 
This is especially useful if you want to disable guest checkout
 but still implement a smooth checkout process. 
 WARNING: You must disable email verification for user accounts.
My settings are:
Store->Configuration->Customer->Customer Configuration:

* Login options -> Redirect to Dashboard after login: No
* Create New Account Options -> Email confirmation: No
* Sales->Checkout->Checkout Options->Allow Guest checkout: No

##Installation:
1. From your root magento 2 installation directory run:
```
composer config repositories.zengoma-magento-autologin git https://Zengoma@bitbucket.org/Zengoma/magento-autologin.git
composer require zengoma/magento-autologin:1.0.4
```
2. Activate the module from the bin directory:
```
./magento module:enable Zengoma_AutoLogin --clear-static-content
```

3. Run setup upgrade from the installation bin directory
```
./magento setup:upgrade
```

##Usage:

Once the module is activated no further setup is required, Customers should automatically be logged in after account creation. 