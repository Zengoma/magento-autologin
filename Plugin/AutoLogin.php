<?php
/**
 * Created by PhpStorm.
 * User: Lance
 * Date: 02/08/2016
 * Time: 16:14
 */

namespace Zengoma\AutoLogin\Plugin;

use Magento\Customer\Model\Session;


class AutoLogin
{
    protected $customerSession;

    public function __construct(
        Session $customerSession
    )
    {
        $this->customerSession = $customerSession;
    }

    public function afterAfterSave(\Magento\Customer\Model\Customer $customer){

        if(!$this->customerSession->isLoggedIn()){

            $this->customerSession->setCustomerAsLoggedIn($customer);

        }

        return $customer;
    }
}